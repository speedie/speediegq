<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>dwm: dynamic window greatness!</title>
<meta charset="UTF-8">
<meta name="description" content="I usually don't do software reviews but I think it's time to start doing some. I've been using dwm since around December after switching from i3 and it really doesn't get any better than dwm. In fact, I haven't switched window manager once since then (other than trying bspwm and some others for a short time).">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>dwm: dynamic window greatness!<h2>
            <h4>2022-06-18</h4>
                <p>Alright so, how do I even start with this one?</p>
                <p>I usually don't do software reviews but I think it's time to start doing some. I've been using dwm since around December after switching from i3 and it really doesn't get any better than dwm. In fact, I haven't switched window manager once since then (other than trying bspwm and some others for a short time).</p>
                <p>dwm is by far the best window manager for GNU/Linux. Many beginners see it as difficult but it's actually really easy once you get the hang of it. And dwm like other suckless programs have very clear benefits which make it so great.</p>
                <p>I've used i3, bspwm and awesome (which wasn't as awesome) and very quickly came to the conclusion that dwm works best for me. <strong>So, why should you use dwm?</strong></p>
                <p>Well, it all comes down to the suckless philosophy which is basically to write minimal software that only comes with the basics. Then, it's up to the users to create patches (in the form of .diff files) to add features to the software. This is an advantage because it means you don't have a bunch of features that you don't want to use in your codebase. But it also means there are no limits in terms of what you can do with the software.</p>
                <p>Most window managers have a configuration file with set options that the window manager can read. This means you're sort of limited in what you can do with the window manager. dwm on the other hand is customized and modified by editing the C source code directly. Not only does this keep the software light and minimal (through not reading a bloated configuration file), but this also means you have no limits in terms of what you can do because it's all C code that can be modified in any shape or form.</p>
                <p>The C programming language is also very minimal meaning you don't need to install a bunch of dependencies (Looking at you, Xmonad and Haskell) so if you run a GNU/Linux distro like Gentoo where everything is compiled from source, you won't have to worry about waiting 2 hours just to get a window manager installed.</p>
                <p>People who are new to dwm and probably use i3 often say it takes a long time to make a change to dwm just because you have to recompile the source code but that's complete nonsense. To install my build of dwm which has way more patches than most people's build, you run 'make clean install' and it's done within one second. Even on older hardware, it's only going to take around 3 seconds.</p>
                <p>But now, let's talk about the window manager itself and the features. Let's start with what it's like right after compiling and installing a unmodified build. You will reach a pretty minimal and plain desktop with a bar at the top.</p>
                <p>In order to spawn a terminal (and make changes to dwm), you press ModKey+Shift+Enter. ModKey is by default 'Alt' but most people change it to the 'Super' so that it does not conflict with keybinds your applications use.</p>
                <p>dwm comes with three layouts by default. A tiling layout (layout 0), a monocle layout (layout 1) and a floating layout (layout 2). Because dwm is a <i>dynamic</i> tiling window manager, windows are managed based on one of these layouts. You can add more through patches but this is what the default build comes with.</p>
                <p>The tiling layout is a 'Master and Stack' layout. Half of the screen is dedicated to the window you last spawned (Master) and the other windows have to share the other half (Stack). The Monocle layout is very different. In the Monocle layout all windows are available but they're stacked on top of each other. The focused window is the only one visible. Finally there's a floating layout where all windows are floating. They can by default be moved around by holding Alt+Left click and dragging it around. Windows are spawned on the top left which is pretty ugly so many users including myself choose to patch in a center patch which means windows spawn in the center of the screen instead.</p>
                <p>Something that I certainly noticed and that you will too if you're coming from i3 or something is that Alt+Shift+q does not kill the focused window but rather your ENTIRE window manager. To actually kill the focused window, you do Alt+Shift+C. This was something I just could not get used to so I ended up changing the keybind. It's definitely something that could be confusing if you're new to dwm.</p>
                <p>As for RAM usage, dwm is basically as good as it gets. This is mostly because dwm is written in C and less than 2000 lines of it. Don't get me wrong though. dwm doesn't have any less features than other window managers. In fact, it actually has MORE because of community provided patches that can be found on the <a href="https://dwm.suckless.org/patches">suckless dwm website.</a>. These can be used with the GNU <code>patch</code> command present on basically every GNU/Linux system and are created with the <code>diff</code> command.</p>
                <p>Patches are basically snippets of code from other people's dwm forks that contain additional features. For example, not everyone wants transparency but if you do, there's an alpha patch available which adds it to dwm. dwm can be as minimal or as bloated as the user wants, that's what's so great about suckless software.</p>
                <p>So, conclusion then. Should you use dwm? If you want an experience that's nearly limitless and allows you the most control then yes. If you're the kind of person who wants the least amount of dependencies or a window manager as fast as it gets, dwm is probably for you.</p>
                <p>However, if you don't like tiling window managers and don't want to control everything using a keyboard then you can just head back to your Ubuntu + GNOME environment that you are so familiar with. As for me, I'm going to continue maintaining <a href="dwm.html">my dwm fork</a> because it has improved my productivity by a lot. My computer is much faster with dwm than it ever was before and consumes much less RAM.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
