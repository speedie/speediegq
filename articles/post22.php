<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>OH NO IM BEING CANCELLED ON TWITTER WHAT WILL I DO???</title>
<meta charset="UTF-8">
<meta name="description" content="A fairly new thing in internet culture recently has been cancelling people. It's a really cringe meme sort of created and spread around by these stupid Twitter users who don't do anything but tweet for 5 hours a day.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>OH NO IM BEING CANCELLED ON TWITTER WHAT WILL I DO???<h2>
            <h4>2022-07-14</h4>
            <p>A fairly new thing in internet culture recently has been "cancelling" people. It's a really cringe meme sort of created and spread around by these stupid Twitter users who don't do anything but tweet for 5 hours a day.</p>
            <p>Let's imagine a scenario where we have Twitter users trying to cancel someone who has an internet presence elsewhere and also uses Twitter:</p>
            <p>Usually what happens is they (as in the stupid twitter users) target someone who has an internet following elsewhere (although this does not have to be the case) who has a public opinion on something that isn't the most common and then try to link it together with something that is now in modern times considered socially unacceptable.</p>
            <p>Then they will attempt to make the general public stop following or supporting them for having this usually pointless opinion "cancelling" the target in question. On the surface, to me, this seems like the usual nonsense created by Twitter users but actually, internet normies keep falling for it instead of being a strong internet chad who stands by their opinions no matter how unique, disgusting, or even illegal they are.</p>
            <p>In modern day internet, it is socially unacceptable to criticize anyone who's even slightly out of the norm in terms of race, gender, sexuality or anything else, even if it's completely unrelated to the problem because they simply will not or cannot seem to stop connecting unrelated issues to people's identity.</p>
            <p>What the targets in this case always seem to miss however is that they are pretty much cancelling themselves. The target will choose to think that THEY are in the wrong and because the Twitter users all follow the norm, they will say the same thing over and over again.</p>
            <p>In reality no one is forcing the target to quit doing what they're doing except themselves. Instead of choosing to stand up for themselves and what they said, they chose to end their online presence because of one (likely small) thing that they said. If you want to avoid getting cancelled, it's really easy. Just do not care about what the stupid Twitter users think about your opinions.</p>
            <p>No one can end your online presence except yourself and the owner of your platform (Twitter, Facebook, YouTube or yourself if you have a website). Acting like you aren't in control of your own social media profiles would be correct. But giving control over YOU and your internet presence to other people is a sign of weakness.</p>
            <p>Simple advice; To avoid being cancelled, simply don't let anyone push you around on the internet, think for yourself and most importantly, stand up for your own opinions and actions, no matter what they.</p>
            <p>That's it for this blog post, have a good day/night!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
