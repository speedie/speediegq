<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Goodbye 2022..</title>
<meta charset="UTF-8">
<meta name="description" content="2023 is basically here, 2022 is (finally) over.. happy new year readers of my blog. Cheers!">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Goodbye 2022..</h2>
                <h4>2022-12-31</h4>
                        <p>2023 is basically here and that means 2022 is over. Happy new year to everyone who follows my blog. March is going to mark 1 year since the first blog post, but there's still a good while until we reach that milestone.</p>
                        <p>Anyhow, I was planning on making a long blog post about 2022 and how.. mixed it was but because I had too much fun, I did not have time to finish it. I didn't really like what I had written anyway so for this year, a small blog post like this one will have to do. What a shame?</p>
                        <p>Nothing is really going to change as 2023 comes around. Blog posts are still going to come occasionally like normal. Either way, since I bothered to make this blog post, I might as well talk about some things that don't really deserve their own blog posts.</p>
                        <p>First of all, mph (the music player project I announced last blog post) is still alive, I did not cancel it like a lot of my other projects. While I had to stop working on it for a bit because I ran out of time and had to fix some critical bugs in other projects, I have implemented a basic plugin system for it now and I'm in the process of implementing basic playback.</p>
                        <p>Secondly, another project of mine, fontctrl got a 1.2 release recently. The release adds support for batch font installation, and can now also take stdin which is probably useful. I highly recommend you try out fontctrl if you're a user of a minimal GNU/Linux system that does not have a GUI font manager already.</p>
                        <p>I have also been making a few (small) changes to speedwm, mostly keybind related. There's now inset support, and it has been updated to the latest suckless commit. Documentation has also been updated and there's now a KeyPress/KeyRelease option which should fix various bugs. Finally rounded corners have been removed because they were buggy and ultimately don't look that good so I removed them. Users who want rounded corners can use picom instead which does rounded corners better than speedwm ever did. Plus it also allows rounded corners for the bar which speedwm never did.</p>
                        <p>speedwm-extras (separate project) was also updated slightly, specifically audioctrl and btctrl, which now work much better.</p>
                        <p>As for the final update, the donate page now has a Vultr referral link, which if you use it will give me $10 in credit or about 1 month of free server uptime. Feel free to use that if you want to support me and get yourself a VPS.</p>
                        <p>Either way, that's it for this blog post, and next blog post (in the next year) will be post number 40! Happy new year everyone, and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
