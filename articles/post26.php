<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Vim: You're wasting your life away if you don't use it.</title>
<meta charset="UTF-8">
<meta name="description" content="Just a short blog post to break the silence. Maybe the title of this blog post is a little rude to people who don't use Vim but it is true. When I say Vim, I don't specifically mean just Vim, there are many good forks of Vim out there such as Neovim which is what I use. However the productivity benefits of using a modular text editor like Vim are so great that I personally believe you're wasting your time (and therefore life) away by not learning and using it.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Vim: You're wasting your life away if you don't use it.</h2>
            <h4>2022-09-04</h4>
            <p>Just a short blog post to break the silence.</p>
            <p>Maybe the title of this blog post is a little rude to people who don't use Vim but it is true. When I say Vim, I don't specifically mean just Vim, there are many good forks of Vim out there such as Neovim which is what I use. However the productivity benefits of using a mode-based text editor like Vim are so great that I personally believe you're wasting your time (and therefore life) away by not learning and using it. In fact this entire website and this page was written in Vim (and later Neovim).</p>
            <p>Vim has actually changed the way I use technology. It may sound strange; how can a "simple" text editor change the way you use your technology? The answer is simple. Vim forces you to learn a bunch of keybinds to get things done. But this is way better because it means if you know what you're doing, you can perform actions REALLY quickly and efficiently.</p>
            <p>As soon as you use Vim, you'll realize how efficient it is and you will try to find and use software that follows the same philosophy. A good example of this is vifm. It's a file manager that tries to be Vim like and therefore efficient. And because you already know Vim, it doesn't have a massive learning curve anymore.</p>
            <p>This is the main reason why so many developers like Vim. It's really efficient when you know how to use it and it doesn't get in your way. For example, to delete the line I am on using a regular GUI text editor, I may use my mouse to select that line and then I will simply delete it. In Vim, I just have to press <code>dd</code> and the line I'm on is instantly gone.</p>
            <p>Another perk of Vim is that it comes preinstalled on almost all GNU/Linux distributions. The only distributions I've used that don't have Vim by default are minimal distributions like Gentoo, Arch, Artix, Void and similar distros.</p>
            <p>This is useful when you're using SSH for example. When you're using SSH, you are usually not able to use GUI applications. Being familiar with a fast, powerful text editor that works in the TTY or through SSH is very important.</p>
            <p>If you want to learn Vim, install Vim or Neovim and run <code>Vimtutor</code> in your terminal. It's not rocket science and anyone can and should do it.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
