<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>I HATE NONFREE SOFTWARE (install gnu icecat)</title>
<meta charset="UTF-8">
<meta name="description" content="As you've read from the blog post title, I hate nonfree software like anyone with a brain should. Problem is, most people don't have a brain. But to fight back against nonfree software I have decided to move over to the 100% free/libre software web browser, GNU IceCat.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>I HATE NONFREE SOFTWARE (install gnu icecat)</h2>
            <h4>2022-09-23</h4>
            <p>As you've read from the blog post title, I hate nonfree software like anyone with a brain should. Problem is, most people don't have a brain. But to fight back against nonfree software I have decided to move over to the 100% free/libre software web browser, GNU IceCat!</p>
            <p>But what makes GNU IceCat special, and why should you use it? Well, truth is we all should use it, we just can't all use it. This is because GNU IceCat blocks <strong>all</strong> nonfree JavaScript.</p>
            <p>IceCat blocks nonfree JavaScript because it allows the programmer to hide malicious features in the code and because it is obfuscated, even developers will struggle to read the code and understand what it does. Reading obfuscated JavaScript is like reading paper that has gone through a paper shredder - it doesn't work.</p>
            <p>Tech companies take advantage of this fact to hide spyware and other tracking in their code. After all, a normie can't read and understand JavaScript code anyway, what are the chances that they will find out what antifeatures the software has?</p>
            <p>To fix this problem, a Mozilla Firefox extension was developed named 'LibreJS'. It checks all JavaScript in a HTML document for a license. If one cannot be found, it will be treated as nonfree and blocked by the extension. This is all done before the website loads so no spyware has time to load on your system.</p>
            <p>GNU IceCat takes this extension, plus adds some fixes caused by missing JavaScript, turns off telemetry (spyware), hardens security and adds some extra extensions. This makes it a bit more usable in comparison to installing Firefox and the extension with nothing else added. It also makes it more secure, because telemetry is disabled and it has been hardened because let's face it, Firefox has terrible default settings, especially for privacy.</p>
            <p>This means, as long as no security settings have been changed, IceCat is one of the best browsers for security and privacy. LibreWolf may have slightly better security, however LibreWolf also doesn't come with LibreJS and has telemetry on by default, just like Firefox so it's really not worth using, you might as well just be using regular Firefox.</p>
            <p>Moving over is not going to be easy, because of the way the modern web works, however I hope that I will be able to use the internet reasonably well, even while blocking nonfree JavaScript. Thank you for reading, and have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
