<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>RIP in peace rchat (and releasing its replacement)</title>
<meta charset="UTF-8">
<meta name="description" content="That's right, I now have a new tool to offer everyone here.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>RIP in peace rchat (and releasing its replacement)</h2>
            <h4>2022-09-21</h4>
                <p>As the title says, I am going to stop developing rchat. I will be accepting pull requests and issues but I am not going to be contributing code to it anymore. It isn't all sad, but the reason for this is because of <a href="https://donut.eu.org/rautafarmi">rautafarmi</a>. rautafarmi recently decided to switch over from the old, crusty messages.txt method to using JSON. This meant rchat would have to be completely rewritten to use this new API, because it was not designed with it in mind.</p>
                <p>Which is why, I am announcing its replacement named 'iron'.</p>
                <p>iron is a fork of rchat but heavily trimmed down, with a clean codebase, much more suckless in design and of course, makes use of the new rautafarmi API. This means it isn't compatible with the older messages.txt method, but I hope instances migrate over to the new JSON API instead.</p>
                <p>iron, unlike rchat also fixes many bugs. In rchat for example, if you disconnect from the internet while its running, rchat will print a bunch of crap on the screen. iron checks if the user is connected to the internet before it attempts to draw anything on the screen and, of course fails if the instance cannot be connected to.</p>
                <p>Like rchat though, iron only comes with the bare minimum in terms of features. For example image sending support is not in it by default, there is a separate patch for it. iron patches can be downloaded and applied and they will extend the feature set of iron. The idea is, give the user a clean, stable base that they can build on top of.</p>
                <p>This is where rchat failed, eventually it got too many features. iron doesn't have auto-updating, it doesn't have image integration, no link opener, no :news, no :changelog and no :help commands. iron only has the bare minimum necessary to send messages and recieve messages by default. It is up to the user to add more features through patches. He only has to apply what he actually uses.</p>
                <p>Because of this philosophy, it is possible to keep iron stable, and alive without constantly pushing bloat to its codebase, requiring even more updates to fix the bugs created.</p>
                <p>Another note I want to add is that iron no longer has a default instance. This keeps the users secure, because if the default instance gets compromised, the default build of iron is not going to compromise its users.</p>
                <p>As of writing this blog post, all rchat patches have been ported over to iron, with additional patches being created. Anyone is allowed to contribute patches that others can apply, as long as they're licensed under the same license as the iron project itself (GNU GPLv3).</p>
                <p>Finally, it should be noted that as of writing this post, the only official instance is the official instance, <a href="https://donut.eu.org/rautafarmi">rautafarmi</a>. If you are using an older instance, ask them to migrate over to the new instance, or keep using the older rchat. I will accept pull requests and issues on rchat for this reason. rchat should be stable enough for regular usage though!</p>
                <p>If you want to try iron, you can download it <a href="iron.html">here</a> and there should be a Gentoo ebuild soon. As the page says, to install iron, clone the repository, cd into it and run <code>make install</code>.</p>
                <p>That is it for this blog post, have a good day!</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
