<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>Notice for spDE users</title>
<meta charset="UTF-8">
<meta name="description" content="Important notice for spDE users running a version pre 2022-03-09.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>Notice for spDE users</h3>
            <h4>2022-03-09</h4>
                <p>This is a notice for spDE users. It now comes with newsboat and this RSS feed built in. There may or may not be a keybind but you can access it from the terminal by running &#39;newsboat&#39;.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
