<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>When will I use BSD?</title>
<meta charset="UTF-8">
<meta name="description" content="So, I am considering installing BSD on my computer, specifically FreeBSD. Or maybe we should call it FreeBaSeD, whatever. Now as you guys know I of course use Gentoo GNU/Linux as my desktop operating system. It's definitely my favorite distribution because it doesn't seem to have all the flaws other distros have. I haven't had many issues with Gentoo, in fact it has been really reliable and I am typing this post from Gentoo. However most other distros don't seem to be very reliable, not to mention some recent events are really encouraging me to switch over to a different operating system.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
</head>
<body>
		<div class="content">
            <h2>When will I use BSD?</h2>
                <h4>2023-01-17</h4>
                    <p>So, I am considering installing BSD on my computer, specifically FreeBSD. Or maybe we should call it FreeBaSeD, whatever.</p>
                    <p>Now as you guys know I of course use Gentoo GNU/Linux as my desktop operating system. It's definitely my favorite distribution because it doesn't seem to have all the flaws other distros like Arch have. I haven't had many issues with Gentoo, I mean Portage is quite slow because it's written in Python but for the most part it has been really reliable and I am typing this post from Gentoo. However most other distros don't seem to be very reliable, not to mention some "recent" events are really encouraging me to switch over to a different operating system.</p>
                    <p>As you all know though, I use free (as in freedom)/libre software and because of this, there's no way I'd use something like Microsoft Windows or Apple macOS©️™️ again but there are many free operating systems, and of course FreeBSD is one of them. I've used OpenBSD in the past, not as a desktop operating system but I use it to host this website. It works perfectly for what I need it to do, but admittedly I'm still kind of Ubuntu user levels of familiar with the BSD operating systems.</p>
                    <p>Either way does this mean I'm going to switch from Gentoo to FreeBSD? Does this mean I'm no longer a Gentoo user? No, it does not, no. But why do I want to use BSD and not GNU/Linux? It's because recently the normies have taken over GNU/Linux and written mandatory garbage for it. One recent example is how Rust (the programming language) is now part of the Linux kernel. While it isn't mandatory yet, drivers and other basic stuff is probably going to be rewritten in it soon, just because the Rust programmers who have no business in the kernel development space can't even write a single line of C. There's now also other stupid software like systemd, Flatpak, Wayland and all this software that takes everything we like about GNU/Linux and throws it in the trash.</p>
                    <p>BSD has some of this stupid software, such as Wayland for example, but for the most part BSD seems pretty clean of the years of trash piled on top of GNU/Linux. I know I've rambled on about how GNU/Linux sucks and I haven't even made this blog post worthwhile, so for real though, why FreeBSD? Well simply, FreeBSD seems to be more like what GNU/Linux was.. you know, 15 years ago before the normies discovered it and ruined it for all of us (yes, even them). It seems to follow the UNIX philosophy much closer than GNU/Linux does these days. I should also add, the BSD community doesn't seem as horrible as the GNU/Linux community, which is of course a plus for various reasons. This may be because normies haven't found it yet, or maybe its users just want a functioning computer. Regardless, it seems like a much better place for someone like me.</p>
                    <p>Either way, this was just a short blog post on why I don't <em>really</em> like GNU/Linux, and why I may consider switching to a system with the BSD kernel. I have another one coming up very soon, as despite not being forced to make blog posts, I still want to put something up so you don't think I'm dead or something. Either way, if you have some experience with BSD, feel free to let me know if there are any huge problems with it but because I use for the most part (Still have the Intel Spyware Engine™️ sadly) free software, software support is not really a huge problem for me.</p>
                    <p>I feel like this post is slowly becoming filler so yeah, might as well end it.</p>
		</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>
