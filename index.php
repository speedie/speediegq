<!DOCTYPE html>
<html>
<head>
<?php include 'php/header.php';?>
<meta charset="UTF-8">
<meta name="description" content="Welcome to my personal website and blog about free software projects and minimalism.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>speedie.gq</title>
</head>
<body>
		<div class="content">
				<h2>Hello there!</h2>
                        <p>Hello, I'm speedie and this is my personal website and blog. I'm a GNU/Linux and free software enthusiast, minimalist and creator of projects like speedwm, spmenu, Forwarder Factory and Project 081. Take a look around!</p>
                        <p>My projects:</p>
                        <ul>
                            <li><a href="https://speedwm.speedie.gq">speedwm</a></li>
                            <li><a href="/projects/libspeedwm.php">libspeedwm</a></li>
                            <li><a href="/projects/spmenu.php">spmenu</a></li>
                            <li><a href="/projects/spde.php">spde</a></li>
                            <li><a href="/projects/fontctrl.php">fontctrl</a></li>
                            <li><a href="/projects/elevendebloater.php">elevendebloater</a></li>
                            <li><a href="/projects/iron.php">iron</a></li>
                            <li><a href="/projects/rchat.php">rchat</a></li>
                            <li><a href="/projects/project081.php">Project 081</a></li>
                            <li><a href="/projects/patches.php">Several patches</a></li>
                            <li><a href="projects.php">More projects</a></li>
                        </ul>
                        <p>If you're still bored, here are some useful links.</p>
						<ul>
							<li><a href="faq.php">Frequently asked questions</a></li>
                                <p>Frequently asked questions are listed here. Please read before you email me asking!</p>
							<li><a href="rss.xml">RSS feed</a></li>
                                <p>RSS feed. All blog posts get fully posted on RSS.</p>
							<li><a href="updates.xml">RSS project updates</a></li>
                                <p>RSS feed for project updates.</p>
							<li><a href="blog.php">Blog</a></li>
                                <p>My blog, usually about free software or personal interests.</p>
							<li><a href="guides.php">Guides</a></li>
                                <p>List of text guides I have written. Most of them are about various GNU/Linux topics.</p>
							<li><a href="projects.php">Projects</a></li>
                                <p>A list of <em>almost</em> all my programming projects.</p>
							<li><a href="projects/overlay.php">Gentoo overlay</a></li>
                                <p>I maintain a Gentoo overlay, that is an unofficial Gentoo repository with custom packages written by myself which extend the packages you can install on a Gentoo system.</p>
							<li><a href="projects/repository.php">Arch repository</a></li>
                                <p>I also maintain an Arch user repository containing almost the same packages.</p>
						</ul>
                        <p>Other services/websites:</p>
                        <ul>
                            <li><a href="https://matrix.to/#/#speedie:matrix.org">Matrix space</a></li>
                                <p>I've been getting into Matrix recently, so feel free to join my space. There's also an end-to-end encrypted space, which is invite only.</p>
                            <li><a href="https://github.com/speediegq">GitHub</a></li>
                                <p>My GitHub account. It is more or less used as a backup, and most of my projects are not primarily hosted there.</p>
                            <li><a href="https://github.com/speediegq">Codeberg</a></li>
                                <p>My Codeberg account. It is more or less used as a backup, and most of my projects are not primarily hosted there.</p>
                            <li><a href="https://www.youtube.com/@speediegq">YouTube</a></li>
                                <p>My YouTube channel. I don't post very often so don't expect any good videos. Most of the content is related to software I've worked on or written.</p>
                        </ul>
				<h3>About this page</h3>
						<p>I am just as concerned about privacy, security and free software as you. Because of my philosophy, this website does not use a single line of JavaScript as I believe it is unnecessary for 90% of what a website needs to do for the average person! This also means, the website is <strong>free/libre (as in freedom)</strong> by design, and therefore also LibreJS compliant. It should be noted that I <em>do</em> use PHP server-side in order to make maintaining this site easier.</p>
						<p>However my entire website, including HTML. CSS, and PHP is available for free and distributed to you under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a> license. <strong>All contributions to the page are licensed under the same license.</strong></p>
						<p>The base for this website was designed by <a href="https://donut.eu.org">emilyd</a> as a concept, and I have expanded on it further. Thank you!</p>
		</div>
</body>
<footer>
		<?php include 'php/footer.php';?>
</footer>
</html>
