#!/bin/sh
[ -z "$1" ] && printf "You must specify a markdown document to convert to HTML.\n"
pandoc -f markdown -t html5 -o "$1.html" "$1" && printf "Converted '%s' to HTML (see '%s.md')\n" "$1" $1" || exit 1
exit 0
