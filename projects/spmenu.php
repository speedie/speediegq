<!DOCTYPE html>
<html>
<head>
<?php include '../php/header.php';?>
<title>spmenu</title>
<meta charset="UTF-8">
<meta name="description" content="speedie's fork of suckless dmenu.">
<meta name="author" content="speedie">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="speedwm.css">
<title>Welcome to speedie.gq</title>
</head>
<body>
		<div class="content">
            <img src="preview2.png" alt="spmenu screenshot" width="75%">
                <h3 id="what-is-spmenu">What is spmenu?</h3>
                    <p>spmenu is an X11 menu application based on <a href="https://tools.suckless.org/dmenu">dmenu</a> which takes standard input, parses it, and lets the user choose an option and sends the selected option to standard output.</p>
                    <p>It is designed to integrate well with my <a href="https://dwm.suckless.org">dwm</a> fork, <a href="https://codeberg.org/speedie/speedwm">speedwm</a>.</p>
                <h3 id="special-features">Special features</h3>
                    <p>This build of spmenu has some features written for this build. Of course if you want, this is free software so you can use it in your own build.</p>
                <ul>
                    <li>dwm-like keybind array (See keybinds.h)</li>
                    <li>256 color support through SGR codes.</li>
                    <li>Option to block typing.</li>
                    <li>Image support (from <a href="https://github.com/Cloudef/dmenu-pango-imlib">this repository</a>)</li>
                    <li>Rewritten arguments, old arguments still work though.</li>
                    <li>Border only when centered option</li>
                    <li>Hiding each part of the menu</li>
                </ul>
                <h3 id="other-features">Other features</h3>
                <ul>
                    <li>Pango markup support</li>
                    <li>Alpha transparency</li>
                    <li>Pywal/.Xresources support</li>
                    <li>Grid</li>
                    <li>Colored Emoji/Font support</li>
                    <li>Highlighting</li>
                    <li>Right-to-left language support</li>
                    <li>Case-insensitive by default</li>
                    <li>Padding; useful with patched dwm with barpadding or speedwm.</li>
                    <li>Fuzzy-finding</li>
                    <li>Preselect support</li>
                    <li>Line-height</li>
                    <li>History support</li>
                    <li>Customizable bar height</li>
                </ul>
                <h3 id="dependencies">Dependencies</h3>
                <ul>
                    <li>libX11</li>
                    <li>libXrender</li>
                    <li>freetype</li>
                    <li>libXinerama
                <ul>
                    <li>Can be disabled if you don’t want/need multi-monitor support.</li>
                </ul></li>
                    <li>tcc compiler (you can swap it out for GCC by passing CC=“gcc” to the <code>make</code> command if you want)</li>
                    <li>Pango (for drawing fonts)
                <ul>
                    <li>Can be disabled if you don't need this.</li>
                </ul></li>
                </ul>
                <h3 id="installation-most-gnulinux-distributions">Installation (most GNU/Linux distributions)</h3>
                    <p><code>emerge dev-vcs/git # Install dev-vcs/git using your favorite package manager</code></p>
                    <p><code>git clone https://codeberg.org/speedie/spmenu</code></p>
                    <p><code>cd spmenu/</code></p>
                    <p><code>make clean install # Run as root.</code></p>
                <h3 id="installation-gentoo">Installation (Gentoo)</h3>
                    <p>If you are on Gentoo GNU/Linux, you can add <a href="overlay.php">my overlay</a> which includes <code>x11-misc/spmenu</code> as well as other useful packages.</p>
                <h3 id="installation-arch">Installation (Arch)</h3>
                    <p>If you are on Arch GNU/Linux, you can add <a href="repository.php">my repository</a> which includes <code>x11-misc/spmenu</code> as well as other useful packages.</p>
                <h3 id="xresources-values">.Xresources values</h3>
                    <p>This build allows you to define .Xresources values to load on
                    startup. See docs/example.Xresources for a list of default values.</p>
                <h3 id="scripts">Scripts</h3>
                    <p>This build of spmenu should work with all spmenu scripts. <a href="https://codeberg.org/speedie/speedwm-extras">Here</a> are a few I’ve written/use.</p>
                <h3 id="notes-for-gcc-users">Notes for GCC users</h3>
                    <p>If you’re compiling with GCC, chances are you’re seeing a lot of warnings. This is because we’re compiling with -Ofast. I can’t seem to find any issues with using -Ofast but if it bothers you, you can compile with -Os or -O2 which don’t spit out these warnings.</p>
</div>
</body>
<footer>
		<?php include '../php/footer.php';?>
</footer>
</html>

