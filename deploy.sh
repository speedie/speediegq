#!/bin/sh
git add *
git commit -a
git push origin master || git push origin main
rsync -av --dry-run * speedie@65.20.115.168:/var/www/page/speedie.gq/
rsync -av --update * speedie@65.20.115.168:/var/www/page/speedie.gq/
