<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/css/footer.css">
<head>
<body>
<div>
<div class="column">
    <h3>This entire website, including <strong>all</strong> HTML. CSS, and PHP is available for free and distributed to you under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a> license. You may get a copy <a href="https://codeberg.org/speedie/speediegq">here.</a></h3>
    <h4>The base for this website was designed by <a href="https://donut.gq">emilyd</a> as a concept, and I have expanded on it further. Thank you!</h4>
</div>
</head>
</html>
